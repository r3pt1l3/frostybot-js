frostybot_exchange_base = require('./exchange.base');

module.exports = class frostybot_exchange_bybit extends frostybot_exchange_base {

    // Class constructor

    constructor(stub) {
        super(stub);
        this.stablecoins = ['USD', 'USDT'];   // Stablecoins supported on this exchange
        this.order_sizing = 'quote';          // Exchange requires base size for orders
        this.collateral_assets = ['XBT'];     // Assets that are used for collateral
        this.balances_market_map = '{currency}/USD'  // Which market to use to convert non-USD balances to USD
        this.param_map = {                   // Order parameter mappings
            limit             : 'Limit',
            market            : 'Market',
            stoploss_limit    : 'StopLimit',
            stoploss_market   : 'Stop',
            takeprofit_limit  : 'Limit',
            takeprofit_market : 'Limit',
        };
        this.trigger_map = {
            'mark'  : 'MarkPrice',
            'last'  : 'LastPrice',
            'index' : 'IndexPrice'
        }
    }

    // Get order parameters

    custom_params(type, order_params, custom_params) {
        if (!order_params.hasOwnProperty('params')) {
            order_params.params = {};
        }
        switch (type) {
            case 'stoploss'         :   order_params.params.stopPx = custom_params.trigger
                order_params.params.execInst = custom_params.reduce ? "ReduceOnly" : "Close"
                order_params.params.execInst += "," + this.trigger_map[custom_params.triggertype]
                break
            case 'takeprofit'       :   order_params.price = order_params.price == null ? custom_params.trigger : null
                //order_params.params.execInst = custom_params.reduce ? "Close" : ""
                break
            default                 :   order_params.params.execInst = custom_params.post ? "ParticipateDoNotInitiate" : ""
        }
        return order_params
    }

    // Get available equity in USD for placing an order on a specific symbol using size as a factor of equity (size=1x)

    async available_equity_usd(symbol) {
        return await this.total_balance_usd();
    }

    // Get list of current positions

    async positions() {

        this.output.debug('other_test', ['bb positions', 'start']);

        this.set_cache_time('private_get_position', 5);
        // let results = await this.ccxt('private_get_position');
        let results = await this.ccxt('v2_private_get_position_list');
        // let results2 = await this.ccxt('private_linear_get_position_list');


        this.output.debug('other_test', ['bb positions results', JSON.stringify(results)]);
        // this.output.debug('other_test', ['bb positions results2', JSON.stringify(results2)]);

        var raw_positions = results;
        await this.markets();
        var positions = [];


        this.output.debug('other_test', ['bb positions raw_positions', JSON.stringify(raw_positions)]);
        if (raw_positions != undefined) {
            this.output.debug('other_test', ['bb positions raw_positions != undefined', 'true!!']);

            var data = raw_positions.result;

            raw_positions.result
                // .filter(raw_position => raw_position.data.size > 0)
                .forEach(async raw_position => {
                    if (raw_position.data.size > 0) {
                        // this.output.debug('other_test', ['bb positions raw_positions', 'start']);
                        // this.output.debug('other_test', ['bb positions raw_position.result', raw_positions.result]);
                        // this.output.debug('other_test', ['bb positions raw_position.result[0]', raw_positions.result[0]]);
                        // this.output.debug('other_test', ['bb positions raw_position.result[0].data', raw_positions.result[0].data]);

                        this.output.debug('other_test', ['bb positions raw_position', JSON.stringify(raw_position)]);

                        const symbol = raw_position.data.symbol;
                        this.output.debug('other_test', ['bb positions symbol', JSON.stringify(symbol)]);

                        const market = await this.get_market_by_id(symbol);
                        this.output.debug('other_test', ['bb positions market', JSON.stringify(market)]);

                        const direction = (raw_position.data.side == 'Sell' ? 'short' : 'long');
                        this.output.debug('other_test', ['bb positions direction', JSON.stringify(direction)]);

                        const base_size = (raw_position.data.position_margin * 1);
                        this.output.debug('other_test', ['bb positions base_size', base_size]);

                        const usd_size = (raw_position.data.size * 1);
                        this.output.debug('other_test', ['bb positions usd_size', JSON.stringify(usd_size)]);

                        const entry_price = (raw_position.data.entry_price * 1);
                        this.output.debug('other_test', ['bb positions entry_price', JSON.stringify(entry_price)]);

                        const liquidation_price = (raw_position.data.liq_price * 1);
                        this.output.debug('other_test', ['bb positions liquidation_price', JSON.stringify(liquidation_price)]);

                        this.output.debug('other_test', ['bb positions market', JSON.stringify(market)]);

                        const raw = raw_position;
                        const position = new this.classes.position_futures(market, direction, base_size, usd_size, entry_price, liquidation_price, raw);
                        this.output.debug('other_test', ['bb positions position', JSON.stringify(position)]);

                        positions.push(position);
                    }
                });
            // await raw_positions
                // .filter(raw_position => raw_position.homeNotional != 0)
                // .forEach(async raw_position => {

                // })
        }

        this.output.debug('other_test', ['bb positions ### ENDE ###', JSON.stringify(positions)]);

        this.positions = positions;
        return this.positions;
    }

    // Get list of markets from exchange

    async markets() {
        if (this.data.markets != null) {
            this.output.debug('other_test', ['bb markets', JSON.stringify(this.data.markets)]);
            return this.data.markets;
        }
        // let results = await this.ccxt('fetch_markets');
        let results = await this.ccxt('load_markets');


        this.output.debug('other_test', ['bb markets2', JSON.stringify(results)]);

        var raw_markets = this.utils.is_array(results) ? results : [];
        this.data.markets = [];
        raw_markets
            .filter(raw_market => raw_market.active == true)
            //.filter(raw_market => raw_market.info.typ == 'FFWCSX')
            .forEach(raw_market => {

                // this.output.debug('other_test', ['bb markets3', JSON.stringify(raw_market)]);

                const id = raw_market.id;
                const symbol = raw_market.symbol;
                const tvsymbol = 'BITMEX:' + raw_market.id;
                const type = raw_market.info.type;
                const base = raw_market.base;
                const quote = raw_market.quote;
                const bid = raw_market.info.bidPrice;
                const ask = raw_market.info.askPrice;
                const expiration = (raw_market.info.expiry != null ? raw_market.info.expiry : null);
                const contract_size = (raw_market.info.contractSize != null ? raw_market.info.contractSize : 1);
                const precision = raw_market.precision;
                const raw = raw_market.info;
                const market = new this.classes.market(id, symbol, type, base, quote, bid, ask, expiration, contract_size, precision, tvsymbol, raw)
                this.data.markets.push(market);
            });
        await this.index_markets();
        await this.update_markets_usd_price();
        return this.data.markets;
    }


    // Get open orders

    async open_orders(params) {
        var [symbol, since, limit] = this.utils.extract_props(params, ['symbol', 'since', 'limit']);
        let raworders = await this.ccxt('fetch_open_orders',[symbol, since, limit]);
        return this.parse_orders(raworders);
    }

    // Get all order history

    async all_orders(params) {
        var [symbol, since, limit] = this.utils.extract_props(params, ['symbol', 'since', 'limit']);
        let raworders = await this.ccxt('fetch_orders',[symbol, since, limit]);
        return this.parse_orders(raworders);
    }

    // Cancel orders

    async cancel(params) {
        var [symbol, id] = this.utils.extract_props(params, ['symbol', 'id']);
        var orders = await this.open_orders({symbol: symbol});
        if (id.toLowerCase() == 'all') {
            let cancel = await this.ccxt('cancel_all_orders',[symbol]);
            orders.forEach((order, idx) => {
                order.status = 'cancelled';
                orders[idx] = order;
            })
        } else {
            orders = orders.filter(order => ['all',order.id].includes(id));
            await orders.forEach(async (order) => {
                var id = order.id;
                let orders = await this.ccxt('cancel_order',[{market: symbol, id: id}]);
            });
            orders.forEach((order, idx) => {
                order.status = 'cancelled';
                orders[idx] = order;
            })
        }
        return orders;
    }


    // Parse CCXT order format into Frostybot order format

    parse_order(order) {
        if (order instanceof this.classes.order) {
            return order;
        }
        const symbol = order.symbol;
        const market = this.data.markets_by_symbol[symbol];
        const id = order.id;
        const timestamp = order.timestamp;
        const direction = order.side;
        const trigger = order.info.stopPx;
        const market_price = (direction == 'buy' ? market.ask : market.bid);
        const price = (order.info.orderPrice != null ? order.info.orderPrice : (order.price != null ? order.price : (trigger != null ? trigger : (type == 'market' ? market_price : null))));
        const size_base = order.amount;
        const size_quote = order.amount * price;
        const filled_base = order.filled;
        const filled_quote = order.filled * price;
        var type = order.type.toLowerCase();
        switch (type) {
            case 'stop'          :  type = (price != trigger ? 'stop_limit' : 'stop_market');
                break;
            case 'take_profit'   :  type = (price != trigger ? 'takeprofit_limit' : 'takeprofit_market');
                break;
        }
        const status = order.status.replace('canceled', 'cancelled');   // Fix spelling error
        const raw = order.info;
        return new this.classes.order(market, id, timestamp, type, direction, price, trigger, size_base, size_quote, filled_base, filled_quote, status, raw);
    }


}
